let style = document.querySelector('style');
style.innerHTML += `.hidden {
            display: none;
        }`;

let ul_list = document.querySelector('.tabs-content');
console.log(ul_list.childNodes);

let li_with_classes = [];
let j = 0;
for (let i = 0; i < ul_list.childNodes.length; i++ ) { 
    if (ul_list.childNodes[i].nodeType === 8) { 
        ul_list.childNodes[i + 2].className = String(ul_list.childNodes[i].data).trim(); 

        // находим все li уже с добавленными классами из комментов.
        li_with_classes[j] = document.querySelector(`.${ul_list.childNodes[i + 2].className}`);
        j++;
    }
}

let btns = document.querySelectorAll('.tabs .tabs-title');

btns.forEach(
    (elem) => {
        elem.addEventListener('click',
            () => {
                // делаем видимым только один li, остальные скрываем
                for (let i = 0; i < li_with_classes.length; i++) {
                    if (li_with_classes[i].classList[0] === `${elem.textContent}`) {
                        li_with_classes[i].classList.remove('hidden');
                    }
                    else {
                        li_with_classes[i].classList.add('hidden');
                    }
                }
                // делаем активной только один tab
                for (let i = 0; i < btns.length; i++) {
                    if (btns[i].textContent === elem.textContent) {
                        btns[i].classList.add('active');
                    }
                    else {
                        btns[i].classList.remove('active');
                    }
                }
            }
        );
    }
);

